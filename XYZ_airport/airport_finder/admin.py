from django.contrib import admin
from .models import Continent, Country, Airport, Flight

# Register your models here.

admin.site.register(Continent)
admin.site.register(Country)
admin.site.register(Airport)
admin.site.register(Flight)