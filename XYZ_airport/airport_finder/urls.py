from django.urls import path, re_path
from . import views


urlpatterns = [
    path('', views.home, name='home'),    
    path('airports/<str:pk_country>/', views.airports, name='airports_list'),
    path('airports/<str:pk_country>/details/<str:pk_airport>/', views.details, name='airport_details'),
]
