from django.shortcuts import render
from .models import Continent, Country, Airport, Flight

# Create your views here.

def home(request):
    #i need to put this in forms.
    europeContinent = Continent.objects.get(continent_name="Europe")
    northAmericaContinent = Continent.objects.get(continent_name="North America")
    centralAmericaContinent = Continent.objects.get(continent_name="Central America")
    caribbeanContinent = Continent.objects.get(continent_name="Caribbean")
    southAmericaContinent = Continent.objects.get(continent_name="South America")
    africaContinent = Continent.objects.get(continent_name="Africa")
    asiaContinent = Continent.objects.get(continent_name="Asia")
    oceaniaContinent = Continent.objects.get(continent_name="Oceania")

    europeCountries = europeContinent.country_set.all()
    northAmericaCountries = northAmericaContinent.country_set.all()
    centralAmericaCountries = centralAmericaContinent.country_set.all()
    caribbeanCountries = caribbeanContinent.country_set.all()
    southAmericaCountries = southAmericaContinent.country_set.all()
    africaCountries = africaContinent.country_set.all()
    asiaCountries = asiaContinent.country_set.all()
    oceaniaCountries = oceaniaContinent.country_set.all()

    countries = {
        'europeCountries':europeCountries, 'northAmericaCountries':northAmericaCountries,
        'centralAmericaCountries':centralAmericaCountries, 'caribbeanCountries':caribbeanCountries,
        'southAmericaCountries':southAmericaCountries, 'africaCountries':africaCountries,
        'asiaCountries':asiaCountries, 'oceaniaCountries':oceaniaCountries
        }

    return render(
        request, 'airport_finder/home.html', countries)


def airports(request, pk_country):
    countriesId = Country.objects.get(id=pk_country)
    airportDetails = countriesId.airport_set.all()
    countryTittle = Country.objects.filter(id=pk_country)

    context = {'countriesId':countriesId, 'airportDetails':airportDetails, 'countryTittle':countryTittle}

    return render(request, 'airport_finder/airports.html', context)


def details(request, pk_airport, pk_country):
    countriesId = Country.objects.get(id=pk_country)
    airportId = Airport.objects.get(airport_id=pk_airport)
    airportTittle = Airport.objects.filter(airport_id=pk_airport)

    context = {'countriesId':countriesId, 'airportId':airportId, "airportTittle":airportTittle}

    return render(request, 'airport_finder/details.html', context)


