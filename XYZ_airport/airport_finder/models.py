from django.db import models

# Create your models here.

class Continent(models.Model):
    CONTINENTS = (
        ('Europe', 'Europe'),
        ('North America', 'North America'),
        ('Central America', 'Central America'),
        ('Caribbean', 'Caribbean'),
        ('South America', 'South America'),
        ('Africa', 'Africa'),
        ('Asia', 'Asia'),
        ('Oceania', 'Oceania'),
    )

    continent_name = models.CharField(max_length=200, null=True, choices=CONTINENTS)

    def __str__(self):
        return self.continent_name


class Country(models.Model):
    country_name = models.CharField(max_length=200)
    continents_id = models.ForeignKey(Continent, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.country_name


class Airport(models.Model):
    DST = (
        ('E', 'E'), ('A', 'A'),
        ('S', 'S'), ('O', 'O'),
        ('Z', 'Z'), ('N', 'N'),
        ('U', 'U'),
    )

    AIRPORT_TYPE = (
        ('airport', 'airport'),
        ('station', 'station'),
        ('port', 'port'),
        ('unknown', 'unknown')
    )

    SOURCE = (
        ('OurAirports', 'OurAirports'),
        ('Legacy', 'Legacy'),
        ('User', 'User'),
    )

    airport_id = models.IntegerField(primary_key=True, null=False)
    airport_name = models.CharField(max_length=200, null=True)
    city = models.CharField(max_length=200, null=True)
    country_id = models.ForeignKey(Country, null=True, on_delete=models.SET_NULL)
    iata = models.CharField(max_length=200, null=True)
    icao = models.CharField(max_length=200, null=True)
    latitude = models.FloatField(null=True)
    longitude = models.FloatField(null=True)
    altitude = models.IntegerField(null=True)
    timezone = models.FloatField(null=True)
    dst = models.CharField(max_length=200, null=True, choices=DST)
    tz_database_time_zone = models.CharField(max_length=200, null=True)
    airport_type = models.CharField(max_length=200, null=True, choices=AIRPORT_TYPE)
    source = models.CharField(max_length=200, null=True, choices=SOURCE)

    def __str__(self):
        return self.airport_name


class Flight(models.Model):
    origin_iata = models.CharField(max_length=200, null=True)
    destination_iata = models.CharField(max_length=200, null=True)
    distance = models.IntegerField(null=True)
    route_iata = models.CharField(max_length=200, null=True)
    airports_id = models.ForeignKey(Airport, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.route_iata