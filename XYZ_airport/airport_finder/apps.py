from django.apps import AppConfig


class AirportFinderConfig(AppConfig):
    name = 'airport_finder'
