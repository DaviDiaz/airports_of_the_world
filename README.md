# airports_of_the_world

# Descripción del Proyecto
“Airports of the World” es una aplicación construida con el framework de desarrollo web “Django”. este permite al usuario obtener de forma rápida y fácil la información de un aeropuerto en específico. También posee un panel administrativo donde gestiona los países, aeropuertos y vuelos de la misma, dándole la facilidad de insertar, modificar o eliminar datos con un entorno gráfico y que posteriormente se despliegue su información en el front-end. Como valor agregado en actualizaciones posteriores se desea integrar componentes que agilicen aún más la búsqueda de aeropuertos y que permita consultar la distancia entre un aeropuerto   y otro. 

#Indicaciones:
Antes de clonar el repositorio necesito que lea la Documentación.
El pdf con el nombre de "Airports_of_theWorld_Doc.pdf" le indicara el proceso paso a paso de como ejecutar el proyecto.

Cualquier consulta escríbame a: ddiazlink08@gmail.com 

-By: David Díaz
