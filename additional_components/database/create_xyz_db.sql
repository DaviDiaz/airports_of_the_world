/*---------------------------------------------------------
		CREATE DATABASE
---------------------------------------------------------*/
CREATE DATABASE XYZ_AIRPORT;
USE XYZ_AIRPORT;

/*---------------------------------------------------------
		TABLE CREATION
---------------------------------------------------------*/

CREATE TABLE `airport_finder_continent` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `continent_name` VARCHAR(200)
);

CREATE TABLE `airport_finder_country` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `country_name` VARCHAR(200),
  `continents_id` int
);

CREATE TABLE `airport_finder_airport` (
  `airport_id` int PRIMARY KEY,
  `airport_name` VARCHAR(200),
  `city` VARCHAR(200),
  `country_id` INT,
  `iata` VARCHAR(200),
  `icao` VARCHAR(200),
  `latitude` FLOAT,
  `longitude` FLOAT,
  `altitude` INT,
  `timezone` FLOAT,
  `dst` VARCHAR(200),
  `tz_database_time_zone` VARCHAR(200),
  `airport_type` VARCHAR(200),
  `source` VARCHAR(200)
);

CREATE TABLE `airport_finder_flight` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `origin_iata` VARCHAR(200),
  `destination_iata` VARCHAR(200),
  `distance` int,
  `route_iata` VARCHAR(200),
  `airports_id` int
);

ALTER TABLE `airport_finder_flight` ADD FOREIGN KEY (`airports_id`) REFERENCES `airport_finder_airport` (`airport_id`);

ALTER TABLE `airport_finder_airport` ADD FOREIGN KEY (`country_id`) REFERENCES `airport_finder_country` (`id`);

ALTER TABLE `airport_finder_country` ADD FOREIGN KEY (`continents_id`) REFERENCES `airport_finder_continent` (`id`);

/*---------------------------------------------------------
    INSERT DATA INTO  "airport_finder_continent" TABLE
---------------------------------------------------------*/
INSERT INTO airport_finder_continent(continent_name)values("Europe");
INSERT INTO airport_finder_continent(continent_name)values("North America");
INSERT INTO airport_finder_continent(continent_name)values("Central America");
INSERT INTO airport_finder_continent(continent_name)values("Caribbean");
INSERT INTO airport_finder_continent(continent_name)values("South America");
INSERT INTO airport_finder_continent(continent_name)values("Africa");
INSERT INTO airport_finder_continent(continent_name)values("Asia");
INSERT INTO airport_finder_continent(continent_name)values("Oceania");

/*---------------------------------------------------------
      INSERT DATA INTO  "airport_finder_country" TABLE
---------------------------------------------------------*/
-->the data is inside the file:
-->"additional_components/database/insert_data/insert_data_country.sql"


/*---------------------------------------------------------
   INSERT DATA INTO  "airport_finder_airport" TABLE
---------------------------------------------------------*/
-->the data is inside the file:
-->"additional_components/database/insert_data/insert_data_airport.sql"


/*---------------------------------------------------------
      INSERT DATA INTO  "airport_finder_flight" TABLE
---------------------------------------------------------*/
-->the data is inside the file:
-->"additional_components/database/insert_data/insert_data_distances.sql"
